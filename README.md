# ![sig2](images/sig2.png) SIG 2 : Les figures du rapport avec QGIS

* durée : 4 jours

* pré-requis : niveau 1 découverte, pratique régulière

  

## Supports de formation

* Cette formation interne à l'Inrap ne dispose pas de support de formation en ligne. En revanche une version pdf est disponible au téléchargement  [![support_sig2](images/pdf.png)](https://drive.google.com/file/d/1rb9WaYOEswQwq0uyhWokVlflBIrir0LQ/view?usp=sharing)

* Pour accéder au Diaporama au cours de la formation : https://slides.com/archeomatic/sig2/live  ou en [consultation libre](https://slides.com/archeomatic/sig2)

  > Se déplacer dans le diaporama  avec les touches ↑↓→← du clavier et la touche [Echap.] pour voir l'ensemble.



## Ressources et documentation

* [Fiches Techniques](https://formationsig.gitlab.io/fiches-techniques)
* [Guide pratique CAVIAR](https://formationsig.gitlab.io/caviar/)
* [Guide pratique des 6 couches (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6MTY2NTliYzRjNzc1Yjk4Yg)
* Note DST - organisation du NAS (à faire)
* [Raccourcis clavier QGIS (pdf)](https://docs.google.com/a/inrap.fr/viewer?a=v&pid=sites&srcid=aW5yYXAuZnJ8cmVzZWF1LXJlZmVyZW50cy1zaWd8Z3g6OTIyMDEyNTlkMTVlYzBl)

### Divers ressources

* Le [pierre à pierre](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/05_pap.html)

* Fiche mémo [Caractériser une variable et  Sémiologie graphique (pdf)](https://drive.google.com/file/d/1CTP9XQoqsgg_aYFmSz3i3crR5RmeF1lf/view?usp=sharing)

  

[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

